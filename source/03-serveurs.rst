Serveurs
========

Echo
-----

.. literalinclude:: code/echo-client.py

.. literalinclude:: code/echo-serveur.py

Un serveur juste avec des sockets
---------------------------------

.. literalinclude:: code/serveur-sockets.py

Comment gérer plusieurs connexions ?

Un serveur juste avec la bibliothèque standard
------------------------------------------------

.. literalinclude:: code/serveur-http.py


* Notre premier "vrai" serveur
* Content-type
* Favicon.ico

Un serveur avec des routes
--------------------------

* Refactors, routes

.. literalinclude:: code/serveur-routes.py

Beaucoup de code pas "utile" ... y aurait-il
une solution plus simple ?
