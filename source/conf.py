project = "Tout comprendre aux sites web"
copyright = "2020, Dimitri Merejkowsky"
author = "Dimitri Merejkowsky"

extensions = ["sphinx.ext.todo"]

release = "0.1"
language = "fr"

templates_path = ["_templates"]
exclude_patterns = []
keep_warnings = True
html_theme = "sphinx_rtd_theme"

todo_include_todos = True

html_show_sourcelink = False
html_static_path = ["_static"]
