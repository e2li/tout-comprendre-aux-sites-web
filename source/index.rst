Tout comprendre aux sites web
==============================

... en en codant un vous-même, de zéro!

Ce qu'on va construire
----------------------

Un moteur de blog avec:

* Des comptes pour les écrivains
* Un accès public aux articles
* Un système de commentaires

Comment ?
----------

En Python. C'est le seul langage que vous avez vraiment besoin de connaître
pour suivre ce guide.

Tous les concepts dont vous avez probablement déjà entendu parler (URL, HTTP,
HTML, requêtes, erreur 404, ...) seront introduits au fur et à mesure.


.. toctree::
   :maxdepth: 1
   :caption: Table des matières

   ./01-sockets.rst
   ./02-http.rst
   ./03-serveurs.rst
   ./04-frameworks.rst
   ./05-flask.rst
   ./06-html.rst
