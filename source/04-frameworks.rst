Frameworks
==========


Une bibliothèque expose des classes et des fonctions. Un
framework te permet d'écrire juste le code minimum et se charge de tout le
reste. Typiquement en Flask, tu ne fais que décrire des routes. Ensuite,
le framework se charge de transformer ces routes en du code qui sait réponde
aux requêtes d'un navigateur. On appelle ça un framework web.


> L'utilisation d'un framework provoque la disparition du main() et un appel
> avec un run externe si j'ai bien compris ?

Pas toujours. Oui dans le cas de flask. Dans le cas de Django par exemple,
tu vas avoir un fichier `manage.py` que tu pourras utiliser pour démarrer
le serveur.


> Peut-on utiliser plusieurs frameworks dans un même programme ?

Oui. Cela dit utiliser deux frameworks web dans le même programme n'est
pas recommandé


> Comment choisit-on un framework ?

Ça dépend de tes besoins, du type de code qui tu fais, de ce dont tu as
l'habitude, de ce qui est à la mode, etc...


> Existe-t-il des bibliothèques des wiki de framework classée selon leur fonction ?
Y a souvent des pages wikipédia qui les listent, oui :
https://en.wikipedia.org/wiki/Category:Python_web_frameworks


Définiton
----------

Quand les utiliser
------------------

Comment les choisir
-------------------

Documentation des frameworks
----------------------------

APIs
+++++

Guides et tutoriels
++++++++++++++++++++
