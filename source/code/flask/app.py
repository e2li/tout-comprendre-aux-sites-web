import os

from flask import Flask

BASE_DES_ARTICLES = "articles/"

app = Flask("mon-serveur")


@app.get("/")
def sert_acceuil():
    contenu = os.listdir(BASE_DES_ARTICLES)
    noms = [x for x in contenu if x.endswith(".txt")]
    noms.sort()
    titre = "Liste des articles:\n\n"
    liste = ""
    for nom in noms:
        liste += "* " + nom[:-4] + "\n"
    return titre + liste

@app.get("/favicon.ico"):
    return flask.send_file("favicon.ico")

class MonGestionnaireDeRequêtes(BaseHTTPRequestHandler):
    def do_GET(self):
        if self.path == "/":
            self.sert_acceuil()
        elif self.path == "/favicon.ico":
            self.sert_icône()
        elif self.path.startswith("/articles/"):
            self.sert_article()
        else:
            self.sert_page_non_trouvée()

    def sert_acceuil(self):
        return self.envoie_réponse(page.encode(), 200, "text/plain")

    def sert_article(self):
        nom = self.path[10:]
        chemin_sur_disque = os.path.join(BASE_DES_ARTICLES, nom + ".txt")
        if not os.path.exists(chemin_sur_disque):
            self.sert_page_non_trouvée()
            return

        with open(chemin_sur_disque, "rb") as f:
            self.envoie_réponse(f.read(), 200, "text/plain ; charset=utf-8")

    def sert_icône(self):
        with open("favicon.ico", "rb") as f:
            data = f.read()
        self.envoie_réponse(data, 200, "img/x-icon")

    def sert_page_non_trouvée(self):
        message = "La page demandée n'a pas été trouvée"
        self.envoie_réponse(message.encode(), 404, "text/plain ; charset=utf-8")

    def envoie_réponse(self, corps, code, type):
        self.send_response(code)
        self.send_header("Content-type", type)
        self.send_header("Content-Length", str(len(corps)))
        self.end_headers()
        self.wfile.write(corps)


class MonServeur(HTTPServer, ThreadingMixIn):
    pass


def main():
    serveur = MonServeur((IP, PORT), MonGestionnaireDeRequêtes)
    print("J'écoute sur ", "http://" + IP + ":" + str(PORT))
    serveur.serve_forever()


if __name__ == "__main__":
    main()
