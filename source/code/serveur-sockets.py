import socket

IP = "127.0.0.1"
PORT = 5000


def parse_requête(requête):
    requête = requête.decode()
    for line in requête.splitlines():
        if line.startswith("GET"):
            mots = line.split()
            return mots[1]


def fabrique_réponse(message):
    encodé = message.encode()
    taille = len(encodé)
    résultat = b"HTTP/1.1 200 OK\r\n"
    résultat += b"Content-Length: " + str(taille).encode() + b"\r\n"
    résultat += b"Content-Type: text/plain; charset=UTF-8\r\n"
    résultat += b"\r\n"
    résultat += encodé
    return résultat


def main():
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as serveur_socket:
        serveur_socket.bind((IP, PORT))
        print("J'écoute sur l'URL: ", "http://" + IP + ":" + str(PORT))
        serveur_socket.listen(1)
        con, add = serveur_socket.accept()
        with con:
            while True:
                message = con.recv(1024)
                if not message:
                    break
                chemin = parse_requête(message)
                print("Le navigateur demande le chemin:", chemin)
                message = fabrique_réponse("Bonjour, ceci est la page " + chemin)
                con.sendall(message)


if __name__ == "__main__":
    main()
