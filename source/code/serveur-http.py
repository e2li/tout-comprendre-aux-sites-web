from http import HTTPStatus
from http.server import BaseHTTPRequestHandler, HTTPServer
from socketserver import ThreadingMixIn
import sys


IP = "127.0.0.1"
PORT = 8080


class MonGestionnaireDeRequêtes(BaseHTTPRequestHandler):
    def do_GET(self):
        print("Chemin demandé:", self.path)
        if self.path == "/":
            message = f"Vous avez demandé la page d'accueil"
            statut = HTTPStatus.OK
        elif self.path.startswith("/articles/"):
            message = f"Vous avez demandé l'article {self.path[10:]}"
            statut = HTTPStatus.OK
        else:
            message = f"Page non trouvée"
            statut = HTTPStatus.NOT_FOUND

        corps = message.encode()
        taille = len(corps)
        self.send_response(statut)
        self.send_header("Content-type", "text/plain; charset=utf-8")
        self.send_header("Content-Length", str(taille))
        self.end_headers()
        self.wfile.write(corps)


class MonServeur(HTTPServer, ThreadingMixIn):
    pass


def main():
    serveur = MonServeur((IP, PORT), MonGestionnaireDeRequêtes)
    print("J'écoute sur ", "http://" + IP + ":" + str(PORT))
    serveur.serve_forever()


if __name__ == "__main__":
    main()
