import socket

IP = "127.0.0.1"
PORT = 3000


with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as client_socket:
    client_socket.connect((IP, PORT))
    while True:
        requête = input()
        client_socket.sendall(requête.encode())
        réponse = client_socket.recv(1024)
        print(réponse.decode())
