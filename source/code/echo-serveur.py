import socket

IP = "127.0.0.1"
PORT = 3000


with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as serveur_socket:
    serveur_socket.bind((IP, PORT))
    serveur_socket.listen(1)
    con, add = serveur_socket.accept()
    with con:
        while True:
            message = con.recv(1024)
            if not message:
                break
            con.sendall(message)
